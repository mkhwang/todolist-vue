# TodoList_Admin

> A Vue.js project

## vue-element-admin copy

- vue-element : [click](https://madewithvuejs.com/element-ui)
- vue-element-admin : [click](https://github.com/PanJiaChen/vue-element-admin) 
- vue-element-admin 템플릿에 있는 패키지들을 전부 포함하였음

## 구성

- vue-element-admin 을 포팅하였다
  - 화면 컴포넌트는 'src/views' 에 위치 시킴
  - 세부 컴포넌트는 기존 'src/components' 에 위치
  - vue-element-admin 의 scss, icon 그대로 사용

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
