import request from '@/utils/request'

export function getMe() {
  return request({
    url: '/me',
    method: 'get'
  })
}

export function modifyMe() {
  return request({
    url: '/me',
    method: 'put'
  })
}

export function deleteMe() {
  return request({
    url: '/me',
    method: 'delete'
  })
}
