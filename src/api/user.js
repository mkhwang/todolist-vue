import request from '@/utils/request'

export function getUserList(keyword) {
  let path = '/admin/users'
  if (keyword !== null) {
    path += '?keyword=' + keyword
  }
  return request({
    url: path,
    method: 'get'
  })
}
