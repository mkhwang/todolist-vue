import request from '@/utils/request'

export function getTaskList(keyword) {
  let path = '/admin/tasks'
  if (keyword !== null) {
    path += '?keyword=' + keyword
  }
  return request({
    url: path,
    method: 'get'
  })
}
