import request from '@/utils/request'

export function loginByUsername(username, password) {
  const data = {
    'password': password,
    'userId': username
  }
  return request({
    url: '/auth/login',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: '/auth/logout',
    method: 'post'
  })
}

export function getUserInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    param: { token }
  })
}

export function checkUserIdDuplicated(userId) {
  return request({
    url: '/auth/user/' + userId,
    method: 'get'
  })
}

export function join(joinForm) {
  const data = {
    'userId': joinForm.userId,
    'password': joinForm.password,
    'mail': joinForm.mail
  }
  return request({
    url: '/auth/join',
    method: 'post',
    data
  })
}
