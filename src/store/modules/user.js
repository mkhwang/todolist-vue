import { loginByUsername, checkUserIdDuplicated, join } from '@/api/login'
import { getMe } from '@/api/me'
// import { logout, getUserInfo } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { Message } from 'element-ui'

const user = {
  state: {
    user: '',
    token: getToken(),
    name: '',
    roles: []
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    }
  },
  actions: {
    LoginByUsername({ commit }, userInfo) {
      const username = userInfo.username.trim()
      return new Promise((resolve, reject) => {
        loginByUsername(username, userInfo.password).then(response => {
          const data = response.data
          if (data.data === null) {
            Message({
              message: 'Invalid UserId or Password',
              type: 'error',
              duration: 3 * 1000
            })
          } else {
            commit('SET_TOKEN', data.data.accessToken)
            setToken(data.data.accessToken)
          }
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },
    Me({ commit, state }) {
      return new Promise((resolve, reject) => {
        getMe().then(response => {
          if (response.data.data.role === 'ROLE_ADMIN') {
            commit('SET_NAME', '관리자')
          } else {
            commit('SET_NAME', '일반사용자')
          }
          commit('SET_ROLES', response.data.data.role)
          const result = { data: { roles: response.data.data.role }}
          resolve(result)
        }).catch(error => {
          reject(error)
        })
      })
    },
    GetUserInfo({ commit, state }) {
      return new Promise((resolve) => {
        commit('SET_ROLES', 'admin')
        commit('SET_NAME', state.name)
        const response = { data: { roles: 'admin' }}
        resolve(response)
      })
      /* @Todo write get user Info api call
      return new Promise((resolve, reject) => {
        getUserInfo(state.token).then(response => {
          if (!response.data) {
            reject('error')
          }
          const data = response.data

          if (data.roles && data.roles.length > 0) {
            commit('SET_ROLES', data.roles)
          } else {
            reject('getInfo: roles must be a non-null array !')
          }

          commit('SET_NAME', data.name)
          // commit('SET_AVATAR', data.avatar)
          // commit('SET_INTRODUCTION', data.introduction)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
      */
    },
    LogOut({ commit, state }) {
      return new Promise((resolve) => {
        commit('SET_TOKEN', '')
        commit('SET_ROLES', [])
        removeToken()
        resolve()
      })
      /* @Todo write logout api call
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '')
          commit('SET_ROLES', [])
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
      */
    },
    CheckUserIdDuplicated({ commit }, userId) {
      return new Promise((resolve, reject) => {
        checkUserIdDuplicated(userId).then(response => {
          const data = response.data
          if (data.data === true) {
            Message({
              message: 'Duplicated UserId',
              type: 'error',
              duration: 3 * 1000
            })
            throw DOMException
          } else {
            Message({
              message: 'OK',
              type: 'success',
              duration: 3 * 1000
            })
          }
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },
    Join({ commit }, joinForm) {
      return new Promise((resolve, reject) => {
        join(joinForm).then(response => {
          Message({
            message: joinForm.userId + ' 님 가입을 축하 합니다.',
            type: 'success',
            duration: 3 * 1000
          })
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default user

