import Vue from 'vue'
import Router from 'vue-router'
const _import = require('./_import_development')

Vue.use(Router)

import Layout from '../views/layout/Layout'

/**
 *  Note: submenu는 children이 1개 이상 있을 때만 보입니다
 *  자세한 내용은 다음을 참고하세요 https://panjiachen.github.io/vue-element-admin-site/#/router-and-nav?id=sidebar
 */

/**
 * hidden: true                   sideBar 에 보이는지 여부 (기본값: false)
 * alwaysShow: true               true 인 경우, 자식 길이 상관없이 루트에 보임
 *                                alwaysShow를 설정하지 않으면 자식 아래 둘 이상의 경로 만 중첩 모드가되고,
 *                                그렇지 않으면 루트 메뉴가 표시되지 않습니다.
 * redirect: noredirect           noredirect로 설정하면 breadcrumb에 redirect하지 않음
 * name:'router-name'             the name is used by <keep-alive> (반드시 설정해야 한다고 함!)
 * meta : {
    roles: ['admin','editor']     will control the page roles (you can set multiple roles)
    title: 'title'                the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'              the icon show in the sidebar,
    noCache: true                 if true ,the page will no be cached(default is false)
  }
 */
export const constantRouterMap = [
  { path: '/login', component: _import('login/index') },
  { path: '/authredirect', component: _import('login/authredirect'), hidden: true },
  { path: '/401', component: _import('errorPage/401'), hidden: true },
  { path: '/404', component: _import('errorPage/404'), hidden: true },
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    children: [{
      path: 'dashboard',
      component: _import('dashboard/index'),
      name: 'dashboard',
      meta: { title: '대쉬보드', icon: 'dashboard', noCache: true }
    }]
  }, {
    path: '/task',
    component: Layout,
    redirect: '/task/index',
    children: [{
      path: 'index',
      component: _import('task/index'),
      name: '작업',
      meta: { title: '작업', icon: 'documentation', noCache: true }
    }]
  }, {
    path: '/me',
    component: Layout,
    redirect: '/me/index',
    children: [{
      path: 'index',
      component: _import('me/index'),
      name: '내 정보',
      meta: { title: '내 정보', icon: 'people', noCache: true }
    }]
  }
]

/**
 * 라우팅 설정에 따라 비동기 식으로 사이드메뉴를 불러옴
 * @type {*[]}
 */
export const asyncRouterMap = [
  {
    path: '/admin',
    component: Layout,
    redirect: '/admin/user',
    alwaysShow: true,
    name: '관리자',
    meta: { title: '관리자', icon: 'password', roles: ['ROLE_ADMIN'] },
    children: [
      {
        path: 'users',
        component: _import('admin/user'),
        name: '사용자관리',
        meta: { title: '사용자관리', icon: 'peoples' }
      },
      {
        path: 'task',
        component: _import('admin/task'),
        name: '작업관리',
        meta: { title: '작업관리', icon: 'component' }
      }
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})
